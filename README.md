# Grab Queue

Downloads multiple sites simultaneously using grab-site or other downloader.

## Dependencies

Works on python 3.7 with the following dependencies met:

    apt install wget python3-tqdm

## Usage

    grab-queue --help
